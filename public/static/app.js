// Customer info elements
const customerNameElement = document.getElementById("customer-name");
const balanceValueElement = document.getElementById("balance-value");
const loanInfoElement = document.getElementById("loan-info");
const loanValueElement = document.getElementById("loan-value");
const getLoanButtonElement = document.getElementById("loan-button");
const repayLoanButtonElement = document.getElementById("repay-button");

// Work elements
const bankButtonElement = document.getElementById("bank-button");
const workButtonElement = document.getElementById("work-button");
const payValueElement = document.getElementById("pay-value");

// Computer
const computerSelectorElement = document.getElementById("laptops");
const computerFeatureListElement = document.getElementById("feature-list");
const computerDetailsHeaderElement = document.getElementById("details-header");
const computerDetailsDecriptionElement = document.getElementById("description");
const computerPriceElement = document.getElementById("price");
const computerImageElement = document.getElementById("computer-image");
const computerBuyElement = document.getElementById("buy-button");

let balance = 0;
let loan = 0;
let totalBalance = balance + loan;
let pay = 0;
let price = 0;

let customerName = "Customer";
customerNameElement.innerText = customerName;

const apiPathBase = "https://noroff-komputer-store-api.herokuapp.com/";

let computerData;
async function getComputerData() {
  try {
    computerData = await fetch(apiPathBase + "computers");
    computerData = await computerData.json();
    computerData.forEach((x) => addComputerToSelector(x));

    addComputerFeatures(computerData[0]);
  } catch (error) {
    console.log(error);
  }
}

const addComputerToSelector = (computer) => {
  const computerOptionElement = document.createElement("option");

  computerOptionElement.value = computer.id;
  computerOptionElement.appendChild(document.createTextNode(computer.title));
  computerSelectorElement.appendChild(computerOptionElement);
};

const addComputerFeatures = (computer) => {
  computerFeatureListElement.innerHTML = "";

  for (const spec of computer.specs) {
    const computerSpecsElement = document.createElement("li");

    computerSpecsElement.value = computer.id;
    computerSpecsElement.appendChild(document.createTextNode(spec));
    computerFeatureListElement.appendChild(computerSpecsElement);
  }

  computerDetailsHeaderElement.innerText = computer.title;
  computerDetailsDecriptionElement.innerText = computer.description;

  price = computer.price;
  computerPriceElement.innerText = price + " kr.";
  computerImageElement.src = apiPathBase + computer.image;
};

getComputerData();

const handleLoan = () => {
  if (balance === 0) {
    alert(
      "You need to have a balancer larger than 0 to be able to apply for a loan."
    );
    return;
  }

  if (loan > 0) {
    alert("You need to pay back your loan before you can apply for a new one.");
    return;
  }

  const maxLoan = balance * 2;
  const loanAmount = parseFloat(
    prompt(`How much do you want to loan? (Max loan ${maxLoan}).`)
  );

  if (loanAmount === NaN || !(loanAmount > 0)) {
    alert("The loan amount needs to be a number larger than 0");
    return;
  }

  if (loanAmount > maxLoan) {
    alert(`You cannot loan more than ${maxLoan} - please try again.`);
    return;
  }

  loan = loanAmount;
  updateTotalBalance();
  loanValueElement.innerText = loan + " kr.";
  loanInfoElement.style.display = "flex";
  balanceValueElement.innerText = totalBalance + " kr.";

  repayLoanButtonElement.style.display = "inline";
  getLoanButtonElement.style.display = "none";
};

const handleRepayLoan = () => {
  if (pay === 0) {
    alert("You need to work, so you have some pay.");
    return;
  }

  if (loan > pay) {
    loan -= pay;
    pay = 0;
  } else {
    pay = pay - loan;
    loan = 0;
  }

  updateTotalBalance();

  payValueElement.innerText = pay + " kr.";
  loanValueElement.innerText = loan + " kr.";

  if (loan === 0) {
    loanInfoElement.style.display = "none";
    repayLoanButtonElement.style.display = "none";
    getLoanButtonElement.style.display = "inline";
  }
};

const handleWork = () => {
  pay += 100;
  payValueElement.innerText = pay + " kr.";
};

const handleBankTransfer = () => {
  if (pay === 0) {
    alert("You need to work, so you have some pay to transfer.");
    return;
  }

  const maxLoanPayment = pay * 0.1;
  if (loan > maxLoanPayment) {
    loan -= maxLoanPayment;
    balance += pay - maxLoanPayment;
  } else {
    balance += pay - loan;
    loan = 0;
  }

  pay = 0;
  updateTotalBalance();

  balanceValueElement.innerText = totalBalance + " kr.";
  payValueElement.innerText = pay + " kr.";
  loanValueElement.innerText = loan + " kr.";

  if (loan === 0) {
    loanInfoElement.style.display = "none";
  }
};

const handleComputerSelectorChange = (e) => {
  const computer = computerData[e.target.selectedIndex];
  addComputerFeatures(computer);
};

const handleBuy = () => {
  if (price > totalBalance) {
    alert(
      `You miss ${
        price - totalBalance
      } kr. - you will need to work some more or maybe take a loan.`
    );
    return;
  }

  balance -= price;
  updateTotalBalance();
  balanceValueElement.innerText = totalBalance + " kr.";
  alert("Congratulation you have bougth a computer");
};

const updateTotalBalance = () => {
  totalBalance = balance + loan;
};

getLoanButtonElement.addEventListener("click", handleLoan);
repayLoanButtonElement.addEventListener("click", handleRepayLoan);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBankTransfer);
computerSelectorElement.addEventListener(
  "change",
  handleComputerSelectorChange
);
computerBuyElement.addEventListener("click", handleBuy);
