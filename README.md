# Computer Store

Computer Store is an example project that shows some JavaScript fundamentals used to create a simple computer store with a bank balance and a means to "earn" money.

You can interact with the example here: https://mortentobiasnielsen.gitlab.io/computerstore/

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have visual studio code installed (or similar text editor/IDE)
* You have node and npm installed

## Installing Computer Store

To install Computer Store, follow these steps:

Open git bash and navigate to the desired location

Paste:
`git clone git@gitlab.com:MortenTobiasNielsen/computerstore.git`

`cd computerstore`

`npm install`

## Using Computer Store

To use Computer Store, follow these steps:

Open the project in your text editor/IDE and run it. 
A websie will open up and you will be able to see your account balance, take a loan, work, and buy a computer if you have enough money

## Contributing to Computer Store
To contribute to Computer Store, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/MortenTobiasNielsen/computerstore/`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

This project uses the following license: [MIT](https://choosealicense.com/licenses/mit/).
